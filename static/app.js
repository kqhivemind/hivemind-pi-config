$('document').ready(() => {
  $('.input.switch .switch-ctr, .input.switch label').click(evt => {
    evt.preventDefault();
    evt.stopPropagation();

    const row = $(evt.target).closest('.input');
    row.toggleClass('checked');
    row.find('input[type=checkbox]').attr('checked', row.hasClass('checked'));
  });

  $('input[type=text]').change(evt => {
    const row = $(evt.target).closest('.input');
    row.toggleClass('empty', evt.target.value === '');
  });

  $('form').submit(evt => {
    evt.preventDefault();
    evt.stopPropagation();

    const form = $(evt.target);
    if (form.hasClass('submitting'))
      return;

    form.hide();
    $('.wait-message').show();

    const values = {};

    for (const input of $('input[type=text]')) {
      values[input.name] = $(input).val();
    }

    for (const input of $('input[type=checkbox]')) {
      values[input.name] = $(input).closest('.input').hasClass('checked');
    }

    for (const select of $('select')) {
      values[select.name] = $(select).val();
    }

    $.ajax({
      type: 'POST',
      url: '/configure',
      data: JSON.stringify(values),
      contentType: 'application/json',
      dataType: 'json',
      success: response => {
        $('.wait-message').hide();
        $('.success-message').show();
      },
      error: (response, status, err) => {
        $('.wait-message').hide();
        form.show().addClass('error');
        form.find('.error-message .message').html(err);
      },
    });
  });
});
